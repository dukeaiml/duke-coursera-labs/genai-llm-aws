"""
Simple invoke claude 2.1 example with boto3 runtime
"""

import boto3
import json

bedrock_runtime_client = boto3.client("bedrock-runtime", region_name="us-west-2")
response = bedrock_runtime_client.invoke_model(
    body=json.dumps(
        {
            "prompt": "Human: What is a healthy fruit that has the color purple\n\nAssistant:",
            "max_tokens_to_sample": 300,
        }
    ),
    modelId="anthropic.claude-v2:1",  # Replace with the full ARN
    contentType="application/json",
)
response_text = response["body"].read().decode("utf-8")
print(response_text)
