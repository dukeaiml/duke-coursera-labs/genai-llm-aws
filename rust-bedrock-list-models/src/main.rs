use aws_sdk_bedrock as bedrock; // Import the AWS Bedrock SDK for Rust

#[tokio::main] // Mark our main function as asynchronous using the Tokio runtime
async fn main() -> Result<(), Box<bedrock::Error>> { // Our main function returns a Result to handle potential errors
    // **Step 1: Load AWS Configuration**
    let config = aws_config::load_from_env().await;
    // Load AWS configuration information from environment variables.

    // **Step 2: Create a Bedrock Client**
    let client = bedrock::Client::new(&config);
    // Create a new client for interacting with the AWS Bedrock service, using the loaded configuration.

    // **Step 3: List Foundation Models**
    let resp = client
        .list_foundation_models() // Initiate the API request to list available foundation models
        .send() // Send the request to the AWS Bedrock service
        .await // Wait for the response from AWS
        .map_err(bedrock::Error::from)?; // Handle potential errors, converting them to our custom 'bedrock::Error' type

    // **Step 4: Access and Print Model Summaries**
    println!("{:#?}", resp.model_summaries);
    // Print the list of 'FoundationModelSummary' structs returned in the response.
    // '{:#?}' is used for pretty-printing, showing details of the data structure.

    // **Step 5: Indicate Success**
    Ok(()) // Return an 'Ok' result from the main function, signaling no errors occurred
}

